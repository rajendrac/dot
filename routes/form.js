var express=require('express');
var router=express.Router();
var Form=require('../model/Form.js');
// var Feedback=require('../model/feedback');
var jwt = require('express-jwt');
var auth = jwt({secret: 'MY_SECRET',userProperty: 'payload'});


var ctrlProfile = require('../controllers/profile');
var ctrlAuth = require('../controllers/authentication');

router.get('/profile', auth, ctrlProfile.profileRead);
router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);
//Get all forms collection
router.get('/',function(req,res,next){
    Form.find(function(err,forms)   //forms it is collection(table) name
     {
       if(err) return next(err);
       res.json(forms);
     });
   });

//get by id   
router.get('/:id',function(req,res,next){
    Form.findById(req.params.id,function(err,post){
      if(err) return next(err);
      res.json(post);
 });
});
   
//Save form
router.post('/',function(req,res,next){
     Form.create(req.body,function(err,post)
     {
         if(err) return next(err);
         res.json(post);
     });
   });


//Update  Form
router.put('/:id',function(req,res,next){
  Form.findByIdAndUpdate(req.params.id,req.body,function(err,post)
  {
      if(err) return next(err);
      res.json(post);
  });
});


//delete Form
 
 router.delete('/:id',function(req,res,next){
  Form.findByIdAndRemove(req.params.id,req.body,function(err,post)
  {
      if(err) return next(err);
      res.json(post);
  });
});

// router.post('/',function(req,res,next){
//   Feedback.create(req.body,function(err,post)
//   {
//       if(err) return next(err);
//       res.json(post);
//   });
// });


module.exports=router;