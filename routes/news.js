var express = require("express");
var router = express.Router();
var news = require("../model/News.js");

//Save feedback

router.get("/", function(req, res, next) {
  news.find(function(
    err,
    news //forms it is collection(table) name
  ) {
    if (err) return next(err);
    res.json(news);
  });
});

//get by id
router.get("/:id", function(req, res, next) {
  news.findById(req.params.id, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

router.post("/", function(req, res, next) {
  news.create(req.body, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

//delete Form

router.delete("/:id", function(req, res, next) {
  news.findByIdAndRemove(req.params.id, req.body, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
