var express = require("express");
var router = express.Router();
var feedback = require("../model/feedback.js");

//Save feedback
router.post("/", function(req, res, next) {
  feedback.create(req.body, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

router.get("/", function(req, res, next) {
  feedback.find(function(
    err,
    feedbacks //forms it is collection(table) name
  ) {
    if (err) return next(err);
    res.json(feedbacks);
  });
});

//   //get by id
router.get("/:id", function(req, res, next) {
  feedback.findById(req.params.id, function(err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

router.delete('/:id',function(req,res,next){
  feedback.findOneAndDelete(req.params.id,req.body,function(err,post)
  {
      if(err) return next(err);
      res.json(post);
  });
});

module.exports = router;
