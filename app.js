// var winston=require('./config/winston')
var express=require('express');
var app=express();
var favicon = require('serve-favicon');
var mongoose=require('mongoose');
var path=require('path');

var logger = require('morgan');
var cookieParser = require('cookie-parser');
var cors = require('cors');
var passport = require('passport');

require('./model/users');
require('./model/feedback');
require('./model/News');
mongoose.Promise=Promise;
mongoose.connect('mongodb://localhost/DeptOTDB',{ useNewUrlParser: true })
  .then(() => console.log('mongodb connection successful'))
  .catch((err)=>console.error(err));
  mongoose.set('useCreateIndex', true)

  
var bodyParser=require('body-parser');
var form=require('./routes/form');
var feedback=require('./routes/feedbacks')
var news=require('./routes/news');

// Routes to Handle Request
const userRoute = require('./routes/user.route')

app.use('/api', userRoute)
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname,'/dist/NameForm')));
app.use('/forms',express.static(path.join(__dirname,'/dist/NameForm')));

app.use('/form',form); // '/form' it is routes name, form is var name
app.use('/feedbacks',feedback);
app.use('/news',news);
var routesApi = require('./routes/form');
app.use('/',routesApi);
require('./config/passport');


app.use('/public', express.static('public'));

// app.use('/',routes);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
// app.use(morgan('combined',{stream:winston.stream}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());
app.use(passport.initialize());

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
 
if (err.name === 'UnauthorizedError') {
  res.status(401);
  res.json({"message" : err.name + ": " + err.message});
}
});

if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
          message: err.message,
          error: err
      });
  });
}


app.use(function(err, req, res, next) {
  // res.locals.message=err.message;
  // res.locals.error=req.app.get('env')==='development'? err:{};
  // winston.error(`${err.status || 500 } - ${err.message }- ${req.originalUrl} - ${req.method} -${req.ip}`);
  res.status(err.status || 500);
  res.render('error', {
      message: err.message,
      error: {}
  });
});



module.exports=app;










