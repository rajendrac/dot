var mongoose = require("mongoose");

var feedbackSchema = new mongoose.Schema({
  name: {
    type: String
  },
  address: {
    type: String
  },
  country: {
    type: String
  },
  email: {
    type: String
  },
  contact: {
    type: String
  },
  altercontact: {
    type: String
  },
  faxno: {
    type: String
  },
  details: {
    type: String
  },
  feedback_Date: {
    type: Date,

    default: Date.now
  }
});

module.exports = mongoose.model("feedback", feedbackSchema);
