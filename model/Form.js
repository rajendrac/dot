//schema it is used in mongo datav=base

// var mongoose =require('mongoose');

// var FormSchema = new mongoose.Schema({
//     Name:String,
//     Address:String, 
//     Gender:String,
//     Hobbies:String,
//     Hobbiess:String,  //String is data type used in database
   
// });

// module.exports=mongoose.model('Form',FormSchema);//Form is a model name


var timestamps = require('mongoose-timestamp');
var mongoose=require('mongoose');
var FormSchema=new mongoose.Schema({
                position:String,
                nameofInsti:String,
                typeofOrg:String,
                typeofOrg1:String,
                    
                contactPerson:String,
                   
                contactNo:String,
                sector:String,
                    
                proposalProjFor:Boolean,
                    
                areaOfImplimentation:Boolean,
                   
                noOfBenificies:String,
                    
                ProposalBudget:String,
                    
                conceptNote:String,
                    
                detailedPlan:String,
                    
                email:String,
                    
                remark:String,
                
                users:[{type:mongoose.Schema.ObjectId,ref:'User',required:true}]

                // time : { type : Date, default: Date.now},
                
                // timestamps : {createdAt:'created_at',updatedAt:'updated_at'}
                // timestamps:true
});
FormSchema.plugin(timestamps);

// or 
//   FormSchema.plugin(timestamps ,{createdAt: 'created_at',
//   updatedAt: 'updated_at'});  
module.exports=mongoose.model('Form',FormSchema);

//PicafSchema is the Schema name written inside the model
/* picafRoute is the router file needed for the routing to the router 
 and picaf is the collection name from the oure database i.e cetral assistance schema */
