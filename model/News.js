var timestamps = require('mongoose-timestamp');
var mongoose=require('mongoose');
var NewsSchema=new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
      type: String
    },
    title_Date:{
        type:Date,
       
        default:Date.now
    },
    avatar: {
        type: Array
      }}, {
        collection: 'news'
      })
NewsSchema.plugin(timestamps);
module.exports=mongoose.model('News',NewsSchema);
