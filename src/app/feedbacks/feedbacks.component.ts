import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-feedbacks',
  templateUrl: './feedbacks.component.html',
  styleUrls: ['./feedbacks.component.css']
})
export class FeedbacksComponent implements OnInit {
  form = {};
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }
  saveFeedback() {
    this.http.post('/feedbacks', this.form)
     .subscribe(res => {
       alert('Submit Feedback Successfully..!');
       this.router.navigate(['/']);
     }, (err) => {
       console.log(err);
     }
     );
  }
}
