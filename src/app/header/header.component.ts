import { Component, OnInit, } from "@angular/core";
import { Subscription } from "rxjs";
import { AuthenticationService } from '../services/authentication.service';

declare var $: any;
declare var require: any;

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit{
  today: number = Date.now();
  constructor(public auth: AuthenticationService) {
    $(document).ready(function () {
      $('.navbar-nav li a').click(function(event) {
        $('.navbar-collapse').collapse('hide');
      });
    });
  }

  ngOnInit() {
  
  }
  refresh(): void {
   window.location.reload();
}

 
}
