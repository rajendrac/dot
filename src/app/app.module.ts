import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { AuthGuardService } from './services/auth-guard.service';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { SliderComponent } from './slider/slider.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthenticationService } from './services/authentication.service';

import { NgxImagesUploadModule } from 'ngx-images-upload';
import { AvatarModule } from 'ngx-avatar';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { NgxPasswordToggleModule } from 'ngx-password-toggle';
import { AboutDOTComponent } from './about-dot/about-dot.component';
import { CoursesComponent } from './courses/courses.component';
import { AdmissionComponent } from './admission/admission.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FacultyComponent } from './faculty/faculty.component';
import { HeaderComponent } from './header/header.component';
import { NewsComponent } from './news/news.component';
import { AddsComponent } from './notifications/adds.component';
import { PowerdComponent } from './powerd/powerd.component';
import { ResearchComponent } from './research/research.component';
import { AdminComponent } from './admin/admin.component';
import { AddNewsComponent } from './add-news/add-news.component';
import { FeedbacksComponent } from './feedbacks/feedbacks.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NewslistComponent } from './newslist/newslist.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'feedbacks',
    component: FeedbacksComponent,
    data: { title: 'Edit List' }
  },

  // { path: 'syllabus', component: SyllabusComponent },
  { path: 'admission', component: AdmissionComponent },

  {
    path: 'faculty',
    component: FacultyComponent,
    data: {
      breadcrumb: 'faculty'
    }
  },
  {
    path: 'news_and_notification',
    component: AddsComponent
  },
  {
    path: 'news',
    component: NewsComponent
  },
  {
    path: 'research',
    component: ResearchComponent
  },
  {
    path: 'about',
    component: AboutDOTComponent
  },
  {
    path: 'contactus',
    component: ContactUsComponent
  },
  {
    path: 'courses',
    component: CoursesComponent
  },
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: 'addnews',
    component: AddNewsComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    FooterComponent,
    SliderComponent,
    AboutDOTComponent,
    CoursesComponent,
    AdmissionComponent,
    ContactUsComponent,
    FacultyComponent,
    HeaderComponent,
    NewsComponent,
    AddsComponent,
    PowerdComponent,
    ResearchComponent,
    AdminComponent,
    AddNewsComponent,
    FeedbacksComponent,
    NewslistComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    // tslint:disable-next-line: deprecation
    HttpModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    NgxImagesUploadModule,
    AvatarModule,
    NgxPasswordToggleModule,
    ShowHidePasswordModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
    // RouterModule.forChild(routes)
  ],
  providers: [AuthenticationService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule {}
