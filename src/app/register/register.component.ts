import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService  } from '../services/authentication.service';
import { TokenPayload } from '../services/TokenPayload';

@Component({
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  credentials: TokenPayload = {
    email: '',
    name: '',
    password: '',
    // designation:''
  };

  constructor(private auth: AuthenticationService, private router: Router) {}

  register() {
    this.auth.register(this.credentials).subscribe(() => {
      this.router.navigateByUrl('/admin');
    }, (err) => {
      console.error(err);
    });
    // console.log("inside the register function in registartion component ts");
  }
}
