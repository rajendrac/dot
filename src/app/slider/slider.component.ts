import { Component } from '@angular/core';
declare var $: any;
declare var require: any;

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent  {

  constructor() {
    $(document).ready(function() {
      $('#myCarousel').swiperight(function() {
        $(this).carousel('prev');
      });
      $('#myCarousel').swipeleft(function() {
        $(this).carousel('next');
      });
    });
   }

  // ngOnInit() {
  // }

}
