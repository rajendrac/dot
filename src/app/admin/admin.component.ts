import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { AbstractControl } from '@angular/forms';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  p: Number = 1;
  forms: {};
  filter: AbstractControl;
  constructor(
    private http: HttpClient,
    private router: Router,
    public auth: AuthenticationService
  ) {}

  ngOnInit() {
    this.http.get('/feedbacks').subscribe(data => {
      /*form  is routes name*/
      this.forms = data;
    });
  }
  deleteForm(id) {
    this.http.delete('/feedbacks/' + id).subscribe(
      res => {
        // confirm('Are you want to Delete Record?');
        this.router.navigate(['/forms']);
      },
      err => {
        console.log(err);
      }
    );
  }
  refresh(): void {
    confirm('Are you want to Delete Record?');
    window.location.reload();
  }
}
