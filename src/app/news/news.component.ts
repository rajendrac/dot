import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FileUploadService } from '../shared/file-upload.service';
import { AbstractControl } from '@angular/forms';
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  forms: {};
  Users: any = [];
  filter: AbstractControl;

  constructor(public fileUploadService: FileUploadService) {
    this.getUsers();
  }
  ngOnInit() {}
  getUsers() {
    this.fileUploadService.getUsers().subscribe(res => {
      this.Users = res['users'];
    });
  }
}
