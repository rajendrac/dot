import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';

declare var $: any;
declare var require: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(public auth: AuthenticationService) {
    $(document).ready(function () {
      $('.navbar-nav li a').click(function(event) {
        $('.navbar-collapse').collapse('hide');
      });
    });
  }
  today: number = Date.now();

 ngOnInit() {

 }
refresh(): void {
  window.location.reload();
}
}
