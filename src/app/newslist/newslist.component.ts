import { Component, OnInit } from '@angular/core';
import { FileUploadService } from '../shared/file-upload.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-newslist',
  templateUrl: './newslist.component.html',
  styleUrls: ['./newslist.component.css']
})
export class NewslistComponent implements OnInit {
  p: Number = 1;
  forms: {};
  Users: any = [];
  filter: AbstractControl;
  constructor(
    public fileUploadService: FileUploadService,
    private http: HttpClient,
    private router: Router
  ) {
    this.getUsers();
  }
  ngOnInit() {}
  getUsers() {
    this.fileUploadService.getUsers().subscribe(res => {
      this.Users = res['users'];
    });
  }

  //   deleteNews(id) {
  //     this.fileUploadService.deleteProduct(id).subscribe(res => {
  //       this.Users.splice(id, 1);
  //     });
  // }

  deleteNews(employee, index) {
    if (window.confirm('Are you sure?')) {
      this.fileUploadService.deleteProduct(employee._id).subscribe(data => {
        this.Users.splice(index, 1);
      });
    }
  }

  refresh(): void {
    alert('you want to Delete Record?');
    window.location.reload();
  }
}
