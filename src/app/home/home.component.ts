import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']

})
export class HomeComponent {


  constructor(public auth: AuthenticationService, private router: Router) {}

}

